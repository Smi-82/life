new Vue({
    el: '#game',
    data: {
        xy: {
            x: 113,
            y: 74
        },
        speed: 0.2,
        arr: [],
        cntAlive: 0,
        maxLive: 0,
        fl: true,
        change_fl: false,
        rep: null,
        archive: '',
        history: []
    },
    mounted: function () {
        this.createGamge();
    },
    methods: {
        changeSpeed: function () {
            try {
                this.rep.stop();
                this.rep.setDelay(parseFloat(this.speed), 's');
                this.rep.start();
            } catch (err) {
            }
        },
        createGamge: function () {
            this.arr = [];
            var arr = new Array(parseInt(this.xy.x));
            arr.fill(false);
            for (var i = 0; i < parseInt(this.xy.y); i++)
                this.arr.push(JSON.parse(JSON.stringify(arr)));
            this.cntAlive = 0;
            this.maxLive = 0;
            this.history = [];
            this.fl = true;
        },
        setTrue: function (y, x) {
            if (this.fl) {
                if (!this.arr[y][x]) {
                    this.$set(this.arr[y], x, true);
                    this.cntAlive++;
                }
            }
        },
        restartGame: function () {
            try {
                this.rep.stop();
                this.createGamge();
            } catch (err) {
            }
        },
        stopGame: function () {
            try {
                this.rep.stop();
            } catch (err) {
            }
            this.fl = true;
        },
        startGame: function () {
            this.fl = false;
            var $this = this;
            var shift = [-1, 0, 1];
            var archive, live, tmp, generation, arr_yx = {x: null, y: null};
            this.xy.y = parseInt(this.xy.y);
            this.xy.x = parseInt(this.xy.x);
            this.rep = new Repeater(function () {
                live = 0;
                generation = [];
                $this.arr.forEach(function (el_y, y) {
                    arr_yx.y = arrFilter('y', y);
                    el_y.forEach(function (status, x) {
                        arr_yx.x = arrFilter('x', x);
                        tmp = check(x, y, getCntLiveNear(arr_yx, x, y));
                        if (status || tmp != status)
                            generation.push({x: x, y: y, status: tmp});
                    });
                });
                generation.forEach(function (el) {
                    if ($this.arr[el.y][el.x] !== el.status)
                        $this.$set($this.arr[el.y], el.x, el.status);
                    if (el.status)
                        live++;
                });

                if (live == 0) {
                    $this.rep.stop();
                    Swal.fire(
                        'Игра окончена',
                        'Все умерли',
                        'error'
                    );
                    $this.fl = true;
                    return false;
                }
                tmp = generation.filter(function (el) {
                    return el.status;
                });
                tmp = tmp.map(function (el) {
                    return el.x + '-' + el.y;
                });
                archive = md5(JSON.stringify(tmp));
                if ($this.archive == archive) {
                    $this.rep.stop();
                    Swal.fire(
                        'Игра окончена',
                        'Новых нет',
                        'error'
                    );
                    $this.fl = true;
                    return false;
                }
                if (in_array(archive, $this.history)) {
                    $this.rep.stop();
                    Swal.fire(
                        'Игра окончена',
                        'Повторилось',
                        'error'
                    );
                    $this.fl = true;
                    return false;
                }
                $this.history.push(archive);
                $this.archive = archive;
                $this.cntAlive = live;
            }, parseFloat(this.speed), 's');
            this.rep.start();
            function getCntLiveNear(arr_yx, x, y) {
                var cnt = 0;
                arr_yx.y.forEach(function (e_y) {
                    arr_yx.x.forEach(function (e_x) {
                        if (!(e_x == x && e_y == y) && $this.arr[e_y][e_x])
                            cnt++;
                    });
                });
                return cnt;
            }

            function check(x, y, cnt) {
                var status = $this.arr[y][x];
                if (!status && cnt == 3) {
                    return true;
                }
                if (cnt > 3 || cnt < 2) {
                    return false;
                }
                return status;
            }

            function arrFilter(key, val) {
                var res = 0;
                return shift.map(function (el) {
                    res = el + val;
                    if (res < 0) {
                        res = $this.xy[key] - 1;
                    }
                    if (res > $this.xy[key] - 1) {
                        res = 0;
                    }
                    return res;
                });
            }
        }
    },
    watch: {
        cntAlive: function () {
            if (this.cntAlive > this.maxLive)
                this.maxLive = this.cntAlive;
        }
    },
    computed: {
        percent: function () {
            return Math.round(this.cntAlive * 100 / (parseInt(this.xy.y) * parseInt(this.xy.x)));
        },
        all: function () {
            return parseInt(this.xy.y) * parseInt(this.xy.x);
        },
        history_cnt: function () {
            return this.history.length;
        }
    }

});
function in_array(needle, arr) {
    return arr.indexOf(needle) != -1;
}
function Repeater(callBacks, delay, key) {
    var _$this = this;
    var _inst = undefined;
    var _d = _createValue(delay, key);
    var _stopCalbacks = {
        isset: false,
        functions: undefined
    };
    var _timer = {
        isset: false,
        delay: 0,
        current_time: 0
    };
    var repeatNumber = {
        isset: false,
        cnt: 0
    };
    this.setRepeatNumber = function (cnt) {
        repeatNumber.isset = true;
        repeatNumber.cnt = cnt;
    };
    this.setStopCallBacks = function (funcs) {
        _stopCalbacks.isset = true;
        _stopCalbacks.functions = funcs;
    };
    this.setTimer = function (t, key_timer) {
        _timer.isset = true;
        _timer.delay = _createValue(t, key_timer);
        _timer.current_time = new Date();
    };
    this.start = function (func) {
        if (callBacks == undefined) {
            throw 'Repeatable functions are not defined';
        }
        if (func != undefined) {
            _repeatCallBacks(func);
        }
        _inst = setInterval(function () {
            _repeatCallBacks(callBacks);
            if (_timer.isset) {
                if ((new Date()) - _timer.current_time >= _timer.delay) {
                    _$this.stop();
                    _timer.isset = false;
                }
            }
            if (repeatNumber.isset) {
                if (!--repeatNumber.cnt) {
                    _$this.stop();
                    repeatNumber.isset = false;
                }
            }
        }, _d);
    };
    this.setDelay = function (val, key) {
        _d = _createValue(val, key);
    };
    this.stop = function (func) {
        clearInterval(_inst);
        _inst = undefined;
        _timer.isset = false;
        if (func != undefined) {
            _repeatCallBacks(func);
        }
        if (_stopCalbacks.isset) {
            _repeatCallBacks(_stopCalbacks.functions);
            _stopCalbacks.isset = false;
            _stopCalbacks.functions = undefined;
        }
    };
    this.isStoped = function () {
        return _inst == undefined;
    };

    function _repeatCallBacks(functions) {
        if (Array.isArray(functions))
            functions.forEach(function (elem) {
                elem();
            });
        else
            functions();
    }

    function _createValue(val, k) {
        switch (k) {
            case 'd':
                val *= 24;
            case 'h':
                val *= 60;
            case 'm':
                val *= 60;
            case 's':
                val *= 1000;
                break;
            default:
                val *= 1;
        }
        return val;
    }
}